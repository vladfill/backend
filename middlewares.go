package main

import (
	"errors"
	"net/http"
	"time"

	"github.com/labstack/echo"
)

func checkUserBan(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		var (
			phone string
			err   error
		)

		// Get phone by token
		token := getHeaderRequestToken(c)
		if token == "" {
			phone = c.FormValue("phone")
		} else {
			phone, err = checkToken(token)
			if checkErr(err) {
				return c.JSON(http.StatusBadRequest, errors.New("Wrong token").Error())
			}
		}

		if checkUserBanByPhone(phone) {
			return next(c)
		}

		return c.JSON(http.StatusBadRequest, errors.New("User have ban or something wrong").Error())
	}
}

// false - is ban enabled
// true - is ban disabled
func checkUserBanByPhone(phone string) bool {
	var (
		user    User
		userBan UserBan
	)

	// Open DB connection
	db, err := openDbConnection()
	defer db.Close()
	if checkErr(err) {
		return false
	}

	// Get User
	db.Where("phone=?", phone).First(&user)

	// Check if user exist
	// If user not exist allow user create account
	if user.ID == 0 {
		return true
	}

	// Get data from user_bans table
	db.Where("user_id=?", user.ID).First(&userBan)
	// Check if userBan for current user exist
	if userBan.ID != 0 {
		// If ExpiredDate not expired deny access
		if userBan.ExpiredDate.Unix()-time.Now().UTC().Unix() > 0 {
			return false
		} else {
			// If ExpiredDate not expired deny access
			// db.Where("user_id=?", user.ID).Delete(&userBan)
			return true
		}
	}

	return true
}
