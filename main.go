package main

import (
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	_ "github.com/lib/pq"
)

// var secret string = randStringBytes(30)
var secret string = "secret"

// jwtClaims - for clients auth
type jwtClaims struct {
	Phone string `json:"phone"`
	jwt.StandardClaims
}

var jwtConfig middleware.JWTConfig = middleware.JWTConfig{
	Claims:     &jwtClaims{},
	SigningKey: []byte(secret),
}

func main() {
	// Echo instance
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.Secure())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE},
	}))
	e.Use(middleware.Static("./"))

	// Route for testing
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})

	// Version 1
	v1 := e.Group("/api/v1")
	v1T := e.Group("/api/v1") // With token
	v1T.Use(middleware.JWTWithConfig(jwtConfig))

	// User Routes
	users := v1.Group("/users")
	users.Use(checkUserBan)
	users.POST("/code", getSMSByPhone)
	users.POST("/login", userLogin)

	usersT := v1T.Group("/users")
	usersT.Use(checkUserBan)
	usersT.GET("", getUsers)
	usersT.GET("/:userID", getUser)
	usersT.PUT("/:userID", updateUser)
	usersT.DELETE("/:userID", deleteUser)
	usersT.POST("/:userID/ban", banUser)
	usersT.POST("/:userID/complaint", complaintUser) // @TODO

	// Media Routes
	media := v1T.Group("/media")
	media.Use(checkUserBan)
	media.POST("", addMedia)
	media.GET("/:mediaID", getMedia)
	media.DELETE("/:mediaID", deleteMedia)

	// Drivers Routes
	drivers := v1T.Group("/drivers")
	drivers.GET("/:driverID", getDriver)
	drivers.POST("", addDriver)
	drivers.POST("/:driverID", updateDriver)
	drivers.DELETE("/:driverID", deleteDriver)
	drivers.POST("/:driverID/merge/:driverUpdateID", mergeDriver)
	drivers.POST("/:driverID/ban", banDriver)  // @TODO
	drivers.GET("/:driverID/cash", cashDriver) // @TODO

	// Orders Routes
	orders := v1T.Group("/orders")
	orders.POST("", addOrder)
	orders.DELETE("/:orderID", removeOrder)

	e.Logger.Fatal(e.Start(":1323"))
}
