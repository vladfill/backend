package main

import (
	"crypto/sha1"
	"encoding/base64"
	"fmt"
	"image"
	"image/color"
	"io/ioutil"
	"log"
	"mime/multipart"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/disintegration/imaging"
)

func savePhoto(file multipart.File, handle *multipart.FileHeader, userID uint) (string, error) {
	data, err := ioutil.ReadAll(file)
	if err != nil {
		return "", err
	}

	// Check if /upload folder is exist
	err = createFolderIfNotExist(uploadPath)
	if err != nil {
		print(err)
	}

	// Check if /upload/:userID folder is exist
	userPath := uploadPath + "/" + strconv.Itoa(int(userID))
	err = createFolderIfNotExist(userPath)
	if err != nil {
		print(err)
	}

	fn := checkIfFileExist(userPath, handle.Filename)
	fp := userPath + "/" + fn

	err = ioutil.WriteFile(fp, data, 0666)
	if err != nil {
		print(err)
		return fp, err
	}
	cropImage(fp)
	return fp, nil
}

func defineCurrentDate(dateType string) string {
	t := time.Now()

	switch dateType {
	case "year":
		return strconv.Itoa(t.Year())
	case "month":
		return strconv.Itoa(int(t.Month()))
	default:
		return strconv.Itoa(t.Nanosecond())
	}
}

func createFolderIfNotExist(path string) error {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		err := os.Mkdir(path, 0777)
		return err
	}

	return nil
}

func checkIfFileExist(path, fileName string) string {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		fmt.Println(err)
	}

	counter := 0
	df := strings.Split(fileName, ".")
	newFileName := getFileNameHash(df[0])

	for _, file := range files {
		dff := strings.Split(file.Name(), ".")
		if strings.HasPrefix(dff[0], newFileName) {
			counter++
		}
	}

	if counter == 0 {
		return newFileName + "." + df[1]
	}

	return newFileName + "-" + strconv.Itoa(counter) + "." + df[1]
}

func cropImage(path string) error {
	src, err := imaging.Open(path)
	if err != nil {
		log.Fatalf("Failed to open image: %v", err)
		return err
	}

	src = imaging.Resize(src, 200, 0, imaging.Lanczos)
	srcW := src.Bounds().Dx()
	srcH := src.Bounds().Dy()
	dst := imaging.New(srcW, srcH, color.NRGBA{0, 0, 0, 0})
	dst = imaging.Paste(dst, src, image.Pt(0, 0))

	err = imaging.Save(dst, path)
	if err != nil {
		log.Fatalf("failed to save image: %v", err)
		return err
	}

	return nil
}

func removeFile(path string) error {
	return os.Remove(path)
}

func getFileNameHash(s string) string {
	h := sha1.New()
	h.Write([]byte(s))

	return base64.URLEncoding.EncodeToString(h.Sum(nil))
}
