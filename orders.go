package main

import (
	"net/http"
	"strconv"
	"time"

	"github.com/labstack/echo"
)

// Order - type for orders database
type Order struct {
	ID           uint      `gorm:"primary_key" db:"id" json:"id"`
	UserID       uint      `db:"user_id" json:"userID"`
	DriverID     uint      `db:"driver_id" json:"driverID"`
	FromAddress  string    `db:"from_address" json:"fromAddress"`
	ToAddress    string    `db:"to_address" json:"toAddress"`
	Coords       string    `db:"coords" json:"coords"`
	Price        float64   `db:"price" json:"price"`
	Distance     float64   `db:"distance" json:"distance"`
	Message      string    `db:"message" json:"message"`
	ChildChair   bool      `db:"child_chair" json:"childChair"`
	Bus          bool      `db:"bus" json:"bus"`
	OrderStatus  string    `db:"order_status" json:"orderStatus"`
	StartedTime  time.Time `db:"started_time" json:"startedTime"`
	FinishedTime time.Time `db:"finished_time" json:"finishedTime"`
	CreatedAt    time.Time
	UpdatedAt    time.Time
	DeletedAt    *time.Time
}

func addOrder(c echo.Context) error {
	var (
		order Order
	)

	// Open DB connection
	db, err := openDbConnection()
	defer db.Close()
	if checkErr(err) {
		return c.JSON(http.StatusInternalServerError, echo.Map{
			"message": "Something went wrong",
		})
	}

	// Get user
	user, _, err := getUserFromRequest(c, db)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, echo.Map{
			"message": "Something went wrong",
		})
	}

	// Get all values
	isFieldsValid := true
	order.UserID = user.ID
	order.OrderStatus = "started"
	order.FromAddress = c.FormValue("from_address")
	order.ToAddress = c.FormValue("to_address")
	order.Coords = c.FormValue("coords")
	price := c.FormValue("price")
	if price != "" {
		order.Price, err = strconv.ParseFloat(price, 64)
		if err != nil {
			isFieldsValid = false
		}
	}

	distance := c.FormValue("distance")
	if distance != "" {
		order.Distance, err = strconv.ParseFloat(distance, 64)
		if err != nil {
			isFieldsValid = false
		}
	}
	order.Message = c.FormValue("message")
	childChair := c.FormValue("child_chair")
	if childChair != "" {
		order.ChildChair, err = strconv.ParseBool(childChair)
		if err != nil {
			isFieldsValid = false
		}
	}

	bus := c.FormValue("bus")
	if bus != "" {
		order.Bus, err = strconv.ParseBool(bus)
		if err != nil {
			isFieldsValid = false
		}
	}

	if !isFieldsValid {
		return c.JSON(http.StatusForbidden, echo.Map{
			"message": "Required fields not filled",
		})
	}

	// Cancel all created orders with status started for current user
	if err := db.Table("orders").Where("user_id=? AND order_status=?", user.ID, "started").
		Updates(map[string]interface{}{"order_status": "canceled"}).Error; err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}

	// Create orders for current user
	if err := db.Create(&order).Error; err != nil {
		return c.JSON(http.StatusInternalServerError, echo.Map{
			"message": err.Error(),
		})
	}

	return nil
}

func removeOrder(c echo.Context) error {
	var (
		order Order
	)
	orderID := c.Param("orderID")

	// Open DB connection
	db, err := openDbConnection()
	defer db.Close()
	if checkErr(err) {
		return c.JSON(http.StatusInternalServerError, echo.Map{
			"message": "Something went wrong",
		})
	}

	// Get user
	user, _, err := getUserFromRequest(c, db)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, echo.Map{
			"message": "Something went wrong",
		})
	}

	if err := db.Table("orders").Where("id=?", orderID).Find(&order).Error; err != nil || order.ID == 0 || user.ID != order.UserID {
		return c.JSON(http.StatusInternalServerError, echo.Map{
			"message": "Order not found",
		})
	}

	if err := db.Table("orders").Where("id=?", orderID).Update("order_status", "canceled").Error; err != nil {
		return c.JSON(http.StatusInternalServerError, echo.Map{
			"message": "Something went wrong",
		})
	}

	db.Delete(&Order{}, orderID)

	return c.JSON(http.StatusNoContent, nil)
}
