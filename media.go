package main

import (
	"net/http"
	"time"

	"github.com/labstack/echo"
)

// Media - type for media database
type Media struct {
	ID        uint   `gorm:"primary_key" db:"id" json:"id"`
	Src       string `db:"src" json:"src"`
	UserID    uint   `db:"user_id" json:"userID"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}

func addMedia(c echo.Context) error {
	var (
		user  User
		media Media
	)

	phone, err := checkToken(getHeaderRequestToken(c))
	if checkErr(err) {
		return c.JSON(http.StatusBadRequest, echo.Map{
			"message": "Wrong token",
		})
	}

	file, fileHeader, err := c.Request().FormFile("image")
	// If request without photo
	if err != nil {
		return c.JSON(http.StatusBadRequest, echo.Map{
			"message": "Could't find image",
		})
	}

	// Open DB connection
	db, err := openDbConnection()
	defer db.Close()
	if checkErr(err) {
		return c.JSON(http.StatusInternalServerError, echo.Map{
			"message": "Something went wrong",
		})
	}

	// Get User
	db.Where("phone=?", phone).First(&user)

	// Check if user exist
	if user.ID == 0 {
		return c.JSON(http.StatusNotFound, echo.Map{
			"message": "User not found",
		})
	}

	// Put image to folder
	src, err := savePhoto(file, fileHeader, user.ID)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, echo.Map{
			"message": "Something went wrong",
		})
	}

	// Save media in DB
	media.UserID = user.ID
	media.Src = src
	if err := db.Create(&media).Error; err != nil {
		return c.JSON(http.StatusInternalServerError, echo.Map{
			"message": "Something went wrong",
		})
	}

	return c.JSON(http.StatusOK, media)
}

func getMedia(c echo.Context) error {
	var (
		user  User
		media Media
	)

	mediaID := c.Param("mediaID")
	phone, err := checkToken(getHeaderRequestToken(c))

	if checkErr(err) {
		return c.JSON(http.StatusBadRequest, echo.Map{
			"message": "Wrong token",
		})
	}

	// Open DB connection
	db, err := openDbConnection()
	defer db.Close()
	if checkErr(err) {
		return c.JSON(http.StatusInternalServerError, echo.Map{
			"message": "Something went wrong",
		})
	}

	// Get User
	db.Where("phone=?", phone).First(&user)

	// Check if user exist
	if user.ID == 0 {
		return c.JSON(http.StatusNotFound, echo.Map{
			"message": "User not found",
		})
	}

	// Get media file by useID and mediaID
	db.Where("id=? AND user_id=?", mediaID, user.ID).Find(&media)

	return c.JSON(http.StatusOK, media)
}

func deleteMedia(c echo.Context) error {
	var (
		user  User
		media Media
	)

	mediaID := c.Param("mediaID")
	phone, err := checkToken(getHeaderRequestToken(c))

	if checkErr(err) {
		return c.JSON(http.StatusBadRequest, echo.Map{
			"message": "Wrong token",
		})
	}

	// Open DB connection
	db, err := openDbConnection()
	defer db.Close()
	if checkErr(err) {
		return c.JSON(http.StatusInternalServerError, echo.Map{
			"message": "Something went wrong",
		})
	}

	// Get User
	db.Where("phone=?", phone).First(&user)

	// Check if user exist
	if user.ID == 0 {
		return c.JSON(http.StatusNotFound, echo.Map{
			"message": "User not found",
		})
	}

	// Get media file by useID and mediaID
	db.Where("id=? AND user_id=?", mediaID, user.ID).Find(&media)

	err = removeFile(media.Src)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, echo.Map{
			"message": "File not found",
		})
	}

	// Delete media file by useID and mediaID
	db.Where("id=? AND user_id=?", mediaID, user.ID).Delete(&media)

	return c.JSON(http.StatusNoContent, nil)
}
