package main

import (
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/labstack/echo"
)

// User - type for users database
type User struct {
	ID        uint      `gorm:"primary_key" db:"id" json:"id"`
	Name      string    `db:"name" json:"name"`
	Phone     string    `db:"phone" json:"phoneNumber"`
	BirthDate time.Time `db:"birth_date" json:"birthDate"`
	UserPhoto string    `db:"user_photo" json:"photo"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}

// UserPhoneCode - codes for valid phone number
type UserPhoneCode struct {
	ID        uint   `gorm:"primary_key" db:"id" json:"id"`
	Code      string `db:"code" json:"code"`
	UserID    uint   `db:"user_id" json:"userID"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}

// UserBan - for admin requests
type UserBan struct {
	ID          uint      `gorm:"primary_key" db:"id" json:"id"`
	UserID      uint      `db:"user_id" json:"userID"`
	ExpiredDate time.Time `db:"expired_date" json:"expiredDate"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   *time.Time
}

func getSMSByPhone(c echo.Context) error {
	var (
		user          User
		userPhoneCode UserPhoneCode
	)

	phone := c.FormValue("phone")
	if !isPhoneValid(phone) {
		return c.JSON(http.StatusForbidden, errors.New("Phone not valid"))
	}

	db, err := openDbConnection()
	defer db.Close()
	if checkErr(err) {
		return c.JSON(http.StatusInternalServerError, err)
	}

	user.Phone = phone
	if err := db.Where("phone=?", phone).FirstOrCreate(&user).Error; err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}

	if user.ID == 0 {
		return c.JSON(http.StatusInternalServerError, nil)
	}

	// code := getRandNumber(1111, 9999)
	code := "1111"

	userPhoneCode.UserID = user.ID
	if err := db.Where("user_id=?", user.ID).FirstOrCreate(&userPhoneCode).Error; err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	if err := db.Model(&userPhoneCode).Where("id=?", userPhoneCode.ID).Update("code", code).Error; err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	if userPhoneCode.ID == 0 {
		return c.JSON(http.StatusInternalServerError, nil)
	}

	return c.JSON(http.StatusOK, user)
}

func userLogin(c echo.Context) error {
	var (
		user          User
		userPhoneCode UserPhoneCode
	)
	timeNow := time.Now().UTC()
	phone := c.FormValue("phone")
	code := c.FormValue("code")

	db, err := openDbConnection()
	defer db.Close()
	if checkErr(err) {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	// Get User
	if err := db.Where("phone=?", phone).First(&user).Error; checkErr(err) {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	// Get userPhoneCode row by User ID
	if err := db.Where("user_id=?", user.ID).First(&userPhoneCode).Error; checkErr(err) {
		return c.JSON(http.StatusInternalServerError, userPhoneCode)
	}

	// If time more that 60 seconds - forbiden
	if timeNow.Unix()-userPhoneCode.UpdatedAt.Unix() > 60 {
		return c.JSON(http.StatusForbidden, errors.New("Time to enter code expired"))
	}

	if code == userPhoneCode.Code {
		token, err := getTokenByPhone(phone, 7)

		if checkErr(err) {
			return c.JSON(http.StatusForbidden, err.Error())
		}

		return c.JSON(http.StatusOK, echo.Map{
			"token": token,
		})
	}

	return c.JSON(http.StatusNotFound, nil)
}

func banUser(c echo.Context) error {
	var userBan UserBan

	expiredDate := c.FormValue("expiredDate")
	userID, err := strconv.Atoi(c.Param("userID"))
	if checkErr(err) {
		return c.JSON(http.StatusBadRequest, errors.New("Wrong user ID"))
	}

	db, err := openDbConnection()
	defer db.Close()
	if checkErr(err) {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	// Try get user from user_bans table
	db.Table("user_bans").Where("user_id=?", userID).Last(&userBan)

	// Get date from expiredDate
	d, err := convertTimestampToDate(expiredDate)
	if checkErr(err) {
		return c.JSON(http.StatusBadRequest, errors.New("Wrong date format"))
	}

	// If not created - create user
	if userBan.UserID == 0 {
		userBan.ExpiredDate = d
		userBan.UserID = uint(userID)
		if err := db.Create(&userBan).Error; err != nil {
			return c.JSON(http.StatusInternalServerError, err.Error())
		}

		return c.JSON(http.StatusAccepted, nil)
	}

	// Update expired date
	userBan.ExpiredDate = d
	if err := db.Model(&userBan).Where("user_id=?", userID).Update("expired_date", d.String()).Error; err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	return c.JSON(http.StatusAccepted, nil)
}

// Get user by ID
func getUser(c echo.Context) error {
	userID, err := strconv.Atoi(c.Param("userID"))
	if checkErr(err) {
		return c.JSON(http.StatusBadRequest, errors.New("Wrong user ID"))
	}

	// Open DB connection
	db, err := openDbConnection()
	defer db.Close()
	if checkErr(err) {
		return c.JSON(http.StatusInternalServerError, err)
	}

	// Get user
	user, _, err := getUserFromRequest(c, db)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, echo.Map{
			"message": "Something went wrong",
		})
	}

	// Check if user exist
	if user.ID == 0 {
		return c.JSON(http.StatusNotFound, nil)
	}

	if user.ID != uint(userID) {
		return c.JSON(http.StatusForbidden, nil)
	}

	return c.JSON(http.StatusOK, user)
}

func getUsers(c echo.Context) error {
	var users []User

	page, err := strconv.Atoi(c.FormValue("page"))
	if checkErr(err) {
		return c.JSON(http.StatusBadRequest, nil)
	}
	quantity, err := strconv.Atoi(c.FormValue("quantity"))
	if checkErr(err) {
		return c.JSON(http.StatusBadRequest, nil)
	}

	// Open DB connection
	db, err := openDbConnection()
	defer db.Close()
	if checkErr(err) {
		return c.JSON(http.StatusInternalServerError, err)
	}

	offset := (page - 1) * quantity
	db.Model(&User{}).Offset(offset).Limit(quantity).Find(&users)

	return c.JSON(http.StatusOK, users)
}

func updateUser(c echo.Context) error {
	mediaID := c.FormValue("media_id")
	birthDate := c.FormValue("birth_date")
	name := c.FormValue("name")

	db, err := openDbConnection()
	defer db.Close()
	if checkErr(err) {
		return c.JSON(http.StatusInternalServerError, nil)
	}

	// Get user
	user, _, err := getUserFromRequest(c, db)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, echo.Map{
			"message": "Something went wrong",
		})
	}

	if user.ID == 0 {
		return c.JSON(http.StatusNotFound, echo.Map{
			"message": "User not found",
		})
	}

	// Get birth date
	d, err := convertTimestampToDate(birthDate)
	if err != nil {
		db.Model(&user).Where("id=?", user.ID).Updates(User{Name: name, UserPhoto: mediaID})
	} else {
		db.Model(&user).Where("id=?", user.ID).Updates(User{Name: name, UserPhoto: mediaID, BirthDate: d})
	}

	return c.JSON(http.StatusNoContent, nil)
}

func deleteUser(c echo.Context) error {
	userID := c.Param("userID")

	db, err := openDbConnection()
	defer db.Close()
	if checkErr(err) {
		return c.JSON(http.StatusInternalServerError, nil)
	}

	db.Delete(&User{}, userID)
	return c.JSON(http.StatusNoContent, nil)
}

func complaintUser(c echo.Context) error {
	return nil
}
