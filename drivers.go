package main

import (
	"net/http"
	"strconv"
	"time"

	"github.com/labstack/echo"
)

// Driver - type for drivers database
type Driver struct {
	ID            uint      `gorm:"primary_key" db:"id" json:"id"`
	UserID        uint      `db:"user_id" json:"user_id"`
	IsConfirmed   bool      `db:"is_confirmed" json:"is_confirmed"`
	CarNumber     string    `db:"car_number" json:"car_number"`
	Brand         string    `db:"brand" json:"brand"`
	Model         string    `db:"model" json:"model"`
	Color         string    `db:"color" json:"color"`
	CarPhoto      string    `db:"car_photo" json:"car_photo"`
	DriverLicense string    `db:"driver_license" json:"driver_license"`
	TechPassport  string    `db:"tech_passport" json:"tech_passport"`
	CarYear       time.Time `db:"car_year" json:"car_year"`
	CreatedAt     time.Time
	UpdatedAt     time.Time
	DeletedAt     *time.Time
}

// DriverUpdate - type for drivers_updates database
type DriverUpdate struct {
	ID            uint      `gorm:"primary_key" db:"id" json:"id"`
	DriverID      uint      `db:"user_id" json:"user_id"`
	CarNumber     string    `db:"car_number" json:"car_number"`
	Brand         string    `db:"brand" json:"brand"`
	Model         string    `db:"model" json:"model"`
	Color         string    `db:"color" json:"color"`
	CarPhoto      string    `db:"car_photo" json:"car_photo"`
	DriverLicense string    `db:"driver_license" json:"driver_license"`
	TechPassport  string    `db:"tech_passport" json:"tech_passport"`
	CarYear       time.Time `db:"car_year" json:"car_year"`
	CreatedAt     time.Time
	UpdatedAt     time.Time
	DeletedAt     *time.Time
}

// DriversCash - type for drivers_cash database
type DriversCash struct {
	ID       uint `gorm:"primary_key" db:"id" json:"id"`
	DriverID uint `db:"user_id" json:"user_id"`
	Balance  int  `db:"balance" json:"balance"`
	Profit   uint `db:"profit" json:"profit"`
}

func getDriver(c echo.Context) error {
	var (
		driver Driver
	)

	// Open DB connection
	db, err := openDbConnection()
	defer db.Close()
	if checkErr(err) {
		return c.JSON(http.StatusInternalServerError, echo.Map{
			"message": "Something went wrong",
		})
	}

	// Get user
	user, _, err := getUserFromRequest(c, db)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, echo.Map{
			"message": "Something went wrong",
		})
	}

	// Get Driver
	if err := db.Where("user_id=?", user.ID).Find(&driver).Error; checkErr(err) || driver.ID == 0 {
		return c.JSON(http.StatusNotFound, echo.Map{
			"message": "Driver not found",
		})
	}

	return c.JSON(http.StatusOK, echo.Map{
		"user":   user,
		"driver": driver,
	})
}

func addDriver(c echo.Context) error {
	var (
		media  Media
		driver Driver
	)
	// Open DB connection
	db, err := openDbConnection()
	defer db.Close()
	if err != nil {
		return c.JSON(http.StatusInternalServerError, echo.Map{
			"message": "Something went wrong",
		})
	}

	// Get user
	user, _, err := getUserFromRequest(c, db)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, echo.Map{
			"message": "Something went wrong",
		})
	}

	// Get text data
	carNumber := c.FormValue("car_number")
	brand := c.FormValue("brand")
	model := c.FormValue("model")
	color := c.FormValue("color")
	carYear := c.FormValue("car_year")

	// check is text data is not empty
	isEmpty := false
	switch {
	case carNumber == "":
		isEmpty = true
	case brand == "":
		isEmpty = true
	case model == "":
		isEmpty = true
	case color == "":
		isEmpty = true
	case carYear == "":
		isEmpty = true
	}

	// Get date from carYear
	d, err := convertTimestampToDate(carYear)
	if checkErr(err) {
		return c.JSON(http.StatusBadRequest, echo.Map{
			"message": "Car year not valid",
		})
	}

	driver.UserID = user.ID
	driver.IsConfirmed = false
	driver.CarNumber = carNumber
	driver.Brand = brand
	driver.Model = model
	driver.Color = color
	driver.CarYear = d

	if isEmpty {
		return c.JSON(http.StatusBadRequest, echo.Map{
			"message": "Not added required filed",
		})
	}

	// Get files
	isFileNotAdded := false
	carPhoto, carPhotoHeader, err := c.Request().FormFile("car_photo")
	if err != nil {
		isFileNotAdded = true
	}

	driverLicense, driverLicenseHeader, err := c.Request().FormFile("driver_license")
	if err != nil {
		isFileNotAdded = true
	}

	techPassport, techPassportHeader, err := c.Request().FormFile("tech_passport")
	if err != nil {
		isFileNotAdded = true
	}

	if isFileNotAdded {
		return c.JSON(http.StatusBadRequest, echo.Map{
			"message": "Could't find image",
		})
	}

	// Save images to User folder and media DB table
	srcCarPhoto, err := savePhoto(carPhoto, carPhotoHeader, user.ID)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, echo.Map{
			"message": "Something went wrong",
		})
	}
	media.UserID = user.ID
	media.Src = srcCarPhoto
	driver.CarPhoto = srcCarPhoto
	db.Create(&media)

	srcLicense, err := savePhoto(driverLicense, driverLicenseHeader, user.ID)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, echo.Map{
			"message": "Something went wrong",
		})
	}
	media.ID = 0
	media.UserID = user.ID
	media.Src = srcLicense
	driver.DriverLicense = srcLicense
	db.Create(&media)

	srcPassport, err := savePhoto(techPassport, techPassportHeader, user.ID)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, echo.Map{
			"message": "Something went wrong",
		})
	}
	media.ID = 0
	media.UserID = user.ID
	media.Src = srcPassport
	driver.TechPassport = srcPassport
	db.Create(&media)

	if err = db.Create(&driver).Error; err != nil {
		return c.JSON(http.StatusInternalServerError, echo.Map{
			"message": err.Error(),
		})
	}

	return c.JSON(http.StatusNoContent, nil)
}

func updateDriver(c echo.Context) error {
	var (
		media        Media
		driver       Driver
		driverUpdate DriverUpdate
	)

	driverID, err := strconv.Atoi(c.Param("driverID"))
	if checkErr(err) {
		return c.JSON(http.StatusBadRequest, echo.Map{
			"message": "Wrong driver ID",
		})
	}

	// Open DB connection
	db, err := openDbConnection()
	defer db.Close()
	if err != nil {
		return c.JSON(http.StatusInternalServerError, echo.Map{
			"message": "Something went wrong",
		})
	}

	// Get user
	user, _, err := getUserFromRequest(c, db)
	if err != nil {
		return c.JSON(http.StatusNotFound, echo.Map{
			"message": "User not found",
		})
	}

	// Get Driver
	if err := db.Where("user_id=?", user.ID).Find(&driver).Error; checkErr(err) || driver.ID == 0 {
		return c.JSON(http.StatusNotFound, echo.Map{
			"message": "Driver not found",
		})
	}

	if uint(driverID) != driver.ID {
		return c.JSON(http.StatusForbidden, echo.Map{
			"message": "Wrong data",
		})
	}

	driverUpdate.DriverID = driver.ID
	driverUpdate.CarNumber = c.FormValue("car_number")
	driverUpdate.Brand = c.FormValue("brand")
	driverUpdate.Model = c.FormValue("model")
	driverUpdate.Color = c.FormValue("color")

	carYear := c.FormValue("car_year")
	// Get date from carYear
	if carYear != "" {
		d, err := convertTimestampToDate(carYear)
		if err != nil {
			return c.JSON(http.StatusBadRequest, echo.Map{
				"message": "Car year not valid",
			})
		}
		driverUpdate.CarYear = d
	}

	// Save images to User folder and media DB table
	carPhoto, carPhotoHeader, err := c.Request().FormFile("car_photo")
	if err == nil {
		src, err := savePhoto(carPhoto, carPhotoHeader, user.ID)
		if err == nil {
			media.UserID = user.ID
			media.Src = src
			driverUpdate.CarPhoto = src
			db.Create(&media)
		}
	}

	driverLicense, driverLicenseHeader, err := c.Request().FormFile("driver_license")
	if err == nil {
		src, err := savePhoto(driverLicense, driverLicenseHeader, user.ID)
		if err == nil {
			media.UserID = user.ID
			media.Src = src
			driverUpdate.DriverLicense = src
			db.Create(&media)
		}
	}

	techPassport, techPassportHeader, err := c.Request().FormFile("tech_passport")
	if err == nil {
		src, err := savePhoto(techPassport, techPassportHeader, user.ID)
		if err == nil {
			media.UserID = user.ID
			media.Src = src
			driverUpdate.TechPassport = src
			db.Create(&media)
		}
	}

	if err := db.Table("drivers_updates").Create(&driverUpdate).Error; err != nil {
		return c.JSON(http.StatusInternalServerError, echo.Map{
			"message": "Error on exist driverUpdate request",
		})
	}

	return c.JSON(http.StatusNoContent, nil)
}

func deleteDriver(c echo.Context) error {
	driverID, err := strconv.Atoi(c.Param("driverID"))
	if checkErr(err) {
		return c.JSON(http.StatusBadRequest, echo.Map{
			"message": "Wrong driver ID",
		})
	}

	db, err := openDbConnection()
	defer db.Close()
	if checkErr(err) {
		return c.JSON(http.StatusInternalServerError, nil)
	}

	db.Delete(&Driver{}, driverID)
	return c.JSON(http.StatusNoContent, nil)
}

func mergeDriver(c echo.Context) error {
	var (
		driver       Driver
		driverUpdate DriverUpdate
	)

	driverID, err := strconv.Atoi(c.Param("driverID"))
	if checkErr(err) {
		return c.JSON(http.StatusBadRequest, echo.Map{
			"message": "Wrong driver ID",
		})
	}

	driverUpdateID := c.Param("driverUpdateID")

	db, err := openDbConnection()
	defer db.Close()
	if checkErr(err) {
		return c.JSON(http.StatusInternalServerError, nil)
	}

	db.Where("id=?", driverUpdateID).Find(&driverUpdate)
	if driverUpdate.ID == 0 || driverUpdate.DriverID != uint(driverID) {
		return c.JSON(http.StatusNotFound, echo.Map{
			"message": "Not found line",
		})
	}

	db.Where("id=?", driverUpdate.DriverID).Find(&driver)
	if driver.ID == 0 {
		return c.JSON(http.StatusNotFound, echo.Map{
			"message": "Not found line",
		})
	}

	updates := make(map[string]interface{})

	if driverUpdate.CarNumber != "" {
		updates["car_number"] = driverUpdate.CarNumber
	}

	if driverUpdate.Brand != "" {
		updates["brand"] = driverUpdate.Brand
	}

	if driverUpdate.Model != "" {
		updates["model"] = driverUpdate.Model
	}

	if driverUpdate.Color != "" {
		updates["color"] = driverUpdate.Color
	}

	if driverUpdate.CarPhoto != "" {
		updates["car_photo"] = driverUpdate.CarPhoto
	}

	if driverUpdate.DriverLicense != "" {
		updates["driver_license"] = driverUpdate.DriverLicense
	}

	if driverUpdate.TechPassport != "" {
		updates["tech_passport"] = driverUpdate.TechPassport
	}

	if !driverUpdate.CarYear.IsZero() {
		updates["car_year"] = driverUpdate.CarYear
	}

	if err := db.Model(&driver).Where("id=?", driverID).Updates(updates).Error; err != nil {
		return c.JSON(http.StatusInternalServerError, echo.Map{
			"message": "Driver not updated",
		})
	}

	return c.JSON(http.StatusNoContent, nil)
}

func banDriver(c echo.Context) error {
	return nil
}

func cashDriver(c echo.Context) error {
	return nil
}
