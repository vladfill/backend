-- Reviews after finished order
-- CREATE TYPE FEEDBACK_FROM AS ENUM ('user', 'driver');

CREATE TABLE IF NOT EXISTS reviews(
  id serial PRIMARY KEY,
  rating SMALLINT NOT NULL,
  feedback VARCHAR (500) NOT NULL,
  feedback_from FEEDBACK_FROM NOT NULL,
  driver_id INT NOT NULL,
  client_id INT NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE,
  deleted_at TIMESTAMP WITH TIME ZONE
);