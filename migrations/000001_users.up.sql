-- All users data
-- CREATE TYPE valid_gender AS ENUM ('male', 'female', 'none_sex');
-- ALTER TYPE valid_gender ADD VALUE 'none';
-- DROP TYPE valid_gender;
-- SET datestyle = dmy;

CREATE TABLE IF NOT EXISTS users(
  id SERIAL PRIMARY KEY,
  name VARCHAR (50),
  phone VARCHAR (50) NOT NULL,
  birth_date TIMESTAMP WITH TIME ZONE,
  user_photo VARCHAR (512),
  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE,
  deleted_at TIMESTAMP WITH TIME ZONE
);

-- INSERT INTO users (first_name, last_name, email, phone, gender)
-- VALUES
--    ('Vlad', 'Filonenko', 'philonenko89@test.com', '+380669087974', 'man');