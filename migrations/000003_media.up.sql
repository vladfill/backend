
CREATE TABLE IF NOT EXISTS media(
  id serial PRIMARY KEY,
  src VARCHAR (100) NOT NULL,
  user_id INTEGER NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE,
  deleted_at TIMESTAMP WITH TIME ZONE
);