
CREATE TABLE IF NOT EXISTS drivers(
  id SERIAL PRIMARY KEY,
  is_confirmed BOOLEAN NOT NULL,
  user_id INTEGER NOT NULL,
  -- name VARCHAR (50),
  -- phone VARCHAR (50) NOT NULL,
  -- birth_date TIMESTAMP WITH TIME ZONE,
  -- driver_photo VARCHAR (512),
  car_number VARCHAR (12),
  brand VARCHAR (20),
  model VARCHAR (20),
  color VARCHAR (20),
  car_year TIMESTAMP WITH TIME ZONE,
  car_photo VARCHAR (512),
  driver_license VARCHAR (512),
  tech_passport VARCHAR (512),
  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE,
  deleted_at TIMESTAMP WITH TIME ZONE
);