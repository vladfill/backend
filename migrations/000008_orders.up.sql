-- Order data
-- CREATE TYPE ORDER_STATUS AS ENUM ('started', 'finished', 'canceled');

CREATE TABLE IF NOT EXISTS orders(
  id serial PRIMARY KEY,
  user_id SMALLINT NOT NULL,
  driver_id SMALLINT,
  from_address TEXT NOT NULL,
  to_address TEXT NOT NULL,
  coords TEXT NOT NULL,
  price NUMERIC(6,2) NOT NULL,
  distance VARCHAR (5) NOT NULL,
  message VARCHAR (200),
  child_chair BOOLEAN NOT NULL,
  bus BOOLEAN NOT NULL,
  order_status ORDER_STATUS NOT NULL,
  started_time TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  finished_time TIMESTAMP WITH TIME ZONE NULL,
  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE,
  deleted_at TIMESTAMP WITH TIME ZONE
);