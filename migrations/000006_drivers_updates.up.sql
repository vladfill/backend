
CREATE TABLE IF NOT EXISTS drivers_updates(
  id SERIAL PRIMARY KEY,
  driver_id INTEGER,
  car_number VARCHAR (12),
  brand VARCHAR (20),
  model VARCHAR (20),
  color VARCHAR (20),
  car_year TIMESTAMP WITH TIME ZONE,
  car_photo VARCHAR (512),
  driver_license VARCHAR (512),
  tech_passport VARCHAR (512),
  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE,
  deleted_at TIMESTAMP WITH TIME ZONE
);