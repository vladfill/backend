package main

import (
	"errors"
	"fmt"
	"log"
	"math/rand"
	"strconv"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo"
	"github.com/ttacon/libphonenumber"

	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func checkErr(err error) bool {
	if err != nil {
		log.Println(err)
		return true
	}

	return false
}

func print(v interface{}) {
	fmt.Println(v)
}

// Will open DB connection
func openDbConnection() (*gorm.DB, error) {
	request := fmt.Sprintf("host=%s port=%d user=postgres dbname=%s sslmode=disable password=%s TimeZone=UTC-0", dbHost, dbPort, dbName, dbPassword)
	db, err := gorm.Open("postgres", request)

	return db, err
}

// Return user and phone by token
func getUserFromRequest(c echo.Context, db *gorm.DB) (User, string, error) {
	var user User
	// Get phone by token
	phone, err := checkToken(getHeaderRequestToken(c))
	if checkErr(err) {
		return user, "", errors.New("Wrong token")
	}

	// Get User
	if err := db.Where("phone=?", phone).Find(&user).Error; checkErr(err) || user.ID == 0 {
		return user, "", errors.New("User not found")
	}

	return user, phone, nil
}

// Random number min - max
func getRandNumber(min, max int) string {
	src := rand.NewSource(time.Now().UnixNano())
	r := rand.New(src)
	n := (r.Intn(max-min) + min)

	return strconv.Itoa(n)
}

// Random string
func randStringBytes(n int) string {
	const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

// check is phone valid
func isPhoneValid(phone string) bool {
	num, err := libphonenumber.Parse(phone, "")
	if checkErr(err) {
		return true
	}

	return libphonenumber.IsValidNumber(num)
}

func checkToken(tokenString string) (string, error) {
	var phone string

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(secret), nil
	})

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		phone = claims["phone"].(string)
		return phone, nil
	}

	return phone, err
}

func getHeaderRequestToken(c echo.Context) string {
	auth := c.Request().Header.Get("Authorization")

	authSplited := strings.Split(auth, " ")
	if len(authSplited) == 2 {
		return authSplited[1]
	}

	return ""
}

func convertBoolStringToInt(v string) bool {
	if v == "true" {
		return true
	}

	return false
}

// Returned token by phone
func getTokenByPhone(phone string, expiredDays uint) (string, error) {
	if isPhoneValid(phone) {
		seconds := fmt.Sprintf("%dh", 24*expiredDays)
		d, err := time.ParseDuration(seconds)
		if checkErr(err) {
			return "", err
		}

		claims := &jwtClaims{
			phone,
			jwt.StandardClaims{
				ExpiresAt: time.Now().Add(d).Unix(),
			},
		}

		// Create token with claims
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

		// Generate encoded token and send it as response.
		t, err := token.SignedString([]byte(secret))
		if checkErr(err) {
			return "", err
		}

		return t, nil
	}

	return "", errors.New("Phone not valid")
}

// Convert JS timestamp to Go time
func convertTimestampToDate(ts string) (time.Time, error) {
	d, err := strconv.ParseInt(ts, 10, 64)
	if checkErr(err) {
		return time.Now(), errors.New("Parse error")
	}

	// Convert nano to ms
	d = d / int64(1000)

	return time.Unix(d, 0), nil
}
